//William Trudel
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes= {new Rectangle(4,3),new Rectangle(9,4),new Circle(3),new Circle(7),new Square(5)}; 
		
		for(int i=0;i<shapes.length;i++) {
			System.out.println("area: "+shapes[i].getArea()+", perimeter: "+shapes[i].getPerimeter());
		}
	}
}
