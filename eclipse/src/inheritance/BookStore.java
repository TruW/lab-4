//William Trudel
package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] store= {new Book("Bible","God"),new ElectronicBook("Bible 2: Electric Boogaloo","Jesus",9000),new Book("Cool Book","Cool Man"),new ElectronicBook("Programming I Guess","Me",2381),new ElectronicBook("Hello","You",666)};
		
		for(int i=0;i<store.length;i++)
			System.out.println(store[i]);

	}

}
