//William Trudel
package inheritance;

public class ElectronicBook extends Book{
	private int numberBytes;

	public ElectronicBook(String title, String author, int numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}

	@Override
	public String toString() {
		String fromBook=super.toString();
		return "EBook: "+fromBook+", size: "+numberBytes;
	}
	

}
